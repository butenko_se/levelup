package org.levelup.lesson6.structure;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class OneWayList<E> implements Structure<E>, Iterable<E> {

    private Element<E> head;
    private int listVersion;

    @Override
    public void add(E value) {
        Element<E> el = new Element<>(value);

        if (head == null){
            //означает, что список пустой
            head = el; //звено теперь является началом списка
        } else {
            Element<E> curr = head;
            while (curr.getNext() != null){
                curr = curr.getNext();
            }
            //curr - будет последний элемент (curr.getNext() = null;)
            curr.setNext(el);
        }
        listVersion++;

    }

    @Override
    public void removeByValue(E value) {

    }

    @Override
    public boolean contains(E value) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
       // return new OneWayListIterator<>(head);
        return new ListIterator();
    }

    private class ListIterator implements Iterator<E>{

        private Element<E> current = head;
        private int iteratorVersion = listVersion;

        @Override
        public boolean hasNext() {
            if (iteratorVersion != listVersion){
                throw new ConcurrentModificationException();
            }
            return current != null;
        }

        @Override
        public E next() {
            if (iteratorVersion != listVersion){
                throw new ConcurrentModificationException();
            }
            E value = current.getValue();
            current = current.getNext();
            return value;
        }
    }

}
