package org.levelup.lesson6.structure;

//1. Все методы абстрактны
//2. Все методы публичны
//3. Все поля имеют вид public static final
//4. У интерфейа нет конструкторов
//5. ОДин класс может реализовывать несколько интерфейсов
//6. Описание того, что может делать ваш класс

public interface Structure<T> {

    void add(T value);

    void removeByValue(T value);

    boolean contains(T value);

}
