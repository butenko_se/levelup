package org.levelup.lesson6.structure;

//Нельзя создать объект этого класса
public abstract class AbstractStructure {

    protected int size; //количество элеменетов в структуре данных

    public AbstractStructure(){}

    public abstract void addLast(int value);

    // Удаление элементов структуры по индексу
    public void removeByIndex(int index){

    }

    public int getByIndex(int index){ return 0; }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

}
