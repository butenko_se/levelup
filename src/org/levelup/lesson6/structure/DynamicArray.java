package org.levelup.lesson6.structure;

// (Динамический массив) Список на основе массива
public class DynamicArray extends AbstractStructure{

    private int[] elementData; // в этом массиве храним значения структуры

    public DynamicArray(int initialCapacity){
        this.elementData = new int[initialCapacity];
    }

    @Override
    public void addLast(int value) {
        if (elementData.length == size){
            //места нет, нужно увеличить размер массива
            int[] oldArray = elementData;
            elementData = new int[(int)(size * 1.5)];

            System.arraycopy(oldArray, 0, elementData, 0, oldArray.length);

        }

        elementData[size++] = value;
        //elementData[size] = value;
        //size = size + 1;

    }

    public void removeByValue(int value){
        for (int i = 0; i < elementData.length - 1; i++){
            if (elementData[i] == value){
                //break;
                for (int j = i; j < size - 1; j++){
                   elementData[j] = elementData[j + 1];
                   size--;
                }
            }
        }


//        for (j = **0**; j < nElems; j++)    {//поиск удаляемого элемента
//            if (arr[j] == searchKey)
//                break;
//                }
//        for (int k = j; k < nElems - 1; k++) //сдвиг последующих элементов
//            arr[k] = arr[k + 1];
//        nElems--;
    }

    public void removeByIndex(int index){
        if (index > elementData.length-1){
            System.out.println("Индекс за пределами массива");
            return;
        }
        for (int i = index; i < size - 1; i++){
            elementData[i] = elementData[i + 1];
            size--;
        }
        //elementData[index] = 0;

    }

    public void trim(){
        if (elementData.length > size){
            //int oldArray[] = elementData;
            int emptyArray[] = new int[elementData.length - size];
            System.arraycopy(elementData, 0, emptyArray, 0, emptyArray.length);
            this.elementData = emptyArray;
        }
    }

}
