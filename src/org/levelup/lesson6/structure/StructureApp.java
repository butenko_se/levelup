package org.levelup.lesson6.structure;

public class StructureApp {

    public static void main(String[] args) {

        Structure<String> list = new OneWayList<>();
        list.add("adlak");
        list.add("ewrjn");
        list.add("adletw");
        list.add("test");

        System.out.println(list);

        AbstractStructure structure = new DynamicArray(5);

        structure.addLast(45); //копировать строку cmd + d
        structure.addLast(65);
        structure.addLast(34);
        structure.addLast(54);
        structure.addLast(78);
        structure.addLast(76);
        structure.addLast(97);
        structure.addLast(12);
        structure.addLast(43);

        DynamicArray dynamicArray = new DynamicArray(4);



    }

}
