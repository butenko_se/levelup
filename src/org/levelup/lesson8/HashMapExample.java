package org.levelup.lesson8;

import org.levelup.lesson8.compare.Phone;
import org.levelup.lesson8.compare.PhonePriceComparator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HashMapExample {

    public static void main(String[] args) {

        HashMap<String, Integer> map = new HashMap<>();

        map.put("Phone", 5);
        map.put("Computer", 10);
        map.put("Monitor", 43);
        map.put("HDD", 22);
        map.put("RAM", 32);

        //Обход коллекции map, первый вариант
        Set<String> keys = map.keySet();
        for (String key: keys){
            Integer value = map.get(key);
            System.out.println("Key: " + key + " Value: " + value);
        }

        //Второй вариант
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for (Map.Entry<String,Integer> entry : entries){
            System.out.println("Key: " + entry.getKey() + " Value: " + entry.getValue());
        }

        Map<Phone, String> treeMap = new TreeMap<>();
        Map<Phone, String> treeMapWithComparator = new TreeMap<>(new PhonePriceComparator());

    }

}
