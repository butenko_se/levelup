package org.levelup.lesson8.compare;

//implements Comparable
//implements Comparator
public class Phone implements Comparable<Phone> {

    private String model;
    private double price;

    public Phone(String model, double price){
        this.model = model;
        this.price = price;
    }

    public double getPrice(){
        return price;
    }

    public String getModel(){
        return model;
    }


    @Override
    public int compareTo(Phone phone) {
        //p1.compareTo(p2) - по аналогии с equals()
        //if p1 < p2 return p1 < 0
        //if p1 == p2 return 0
        //if p1 > p2 return p1 > 0
        return model.compareTo(phone.model);
    }
}
