package org.levelup.lesson8.exeptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    public Date convertStringToDate(String dateAsString){
        //Date - количество милисекунд, которые прошли с 1 января 1970 года с 00:00

        //d - from 1 - 9 -> 1 - 9
        //dd - from 1 - 9 -> 01 - 09
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss"); //аргумент в данном случае - формат даты

        try {
            System.out.println("Before parse");
            Date date = formatter.parse(dateAsString);
            System.out.println("After parse");
            return date;
        } catch (ParseException exc){
            System.out.println("Не правильный формат даты!");
            exc.printStackTrace();
            return null;
        }
    }

}
