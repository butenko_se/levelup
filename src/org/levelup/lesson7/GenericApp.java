package org.levelup.lesson7;

public class GenericApp {

    public static void main(String[] args) {
        GenericObject<String> object = new GenericObject<>();
        object.setField("");

        GenericObject<Integer> integerGenericObject = new GenericObject<>();
        integerGenericObject.setField(405);

        GenericObject<Object> objectGenericObject = new GenericObject<>();
        objectGenericObject.setField(new Object());

        ////////////////

        GenericPair<String, Integer> stringPairs = new GenericPair<>();
        stringPairs.getPairByFirstValue("");
        stringPairs.getPairBySecondValue(12);

    }

}
