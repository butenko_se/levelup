package org.levelup.lesson7;

import java.util.ArrayList;
import java.util.List;

public class CollectionExamples {

    public static void main(String[] args) {

        List<String> words = new ArrayList<>();
        words.add("Hello");
        words.add("Java");
        words.add("Payments");
        words.add("Presentation");
        words.add("Analyze");

        //foreach
        //for(<Generic type> <var> : <collection Object>)
        for (String word : words){
            //НЕЛЬЗЯ! добавлять или удалять элементы коллекции
            System.out.println(word);
        }
        System.out.println("Collection size: " + words.size());

        //start - included
        //end - excluded
        List<String> subWords = words.subList(1, 4);
        for (String subWord: subWords){
            System.out.println(subWord);
        }

        String firstWord = words.get(0);
        System.out.println("First element " + firstWord);

    }

}
