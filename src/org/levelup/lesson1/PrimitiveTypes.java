package org.levelup.lesson1;

public class PrimitiveTypes {

    public static void main(String[] args){
/*
//        // <тип переменной> <название переменной>
//        int countOfProducts;
//        countOfProducts = 364; //присвоили переменной значение
//        System.out.println(countOfProducts);
//
//        int price = 5834; //инициализация переменной
//        int total = price * countOfProducts;
//        System.out.println("Итого: " + total);
*/

        firstStep();
        secondStep();
    }

    public static void firstStep(){

        int a = 2;
        double b = 3;

        System.out.println(a + b);
        System.out.println(a - b);
        System.out.println(a * b);
        System.out.println(a / b);

    }

    public static void secondStep(){

        int a, b;
        a = 6;
        b = 24;
        double x = b / a;
        System.out.println(x);

    }

}
