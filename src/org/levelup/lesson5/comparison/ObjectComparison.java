package org.levelup.lesson5.comparison;

public class ObjectComparison {

    public static void main(String[] args) {

        Computer c1 = new Computer("",8);
        Computer c2 = new Computer("", 8);

        System.out.println("c1 == c2: " + (c1 == c2));

        boolean isEqual = c1.equals(c2);

    }

}
