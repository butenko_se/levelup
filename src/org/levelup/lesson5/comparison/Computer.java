package org.levelup.lesson5.comparison;

import java.util.Objects;

public class Computer {

    private int ram;
    private long diskSize;
    private String brand;

    public Computer(String brand, int ram){
        this.brand = brand;
        this.ram = ram;
    }

    // getter
    // public <field_type> get<FieldName>
    public int getRam(){
        return ram;
    }

    // setter
    public void setRam(int ram){
        this.ram = ram;
    }

    public void setDiskSize(long diskSize) {
        this.diskSize = diskSize;
    }

    public void changeRAM(int ram){
        if (ram > 0){
            this.ram = ram; //в таком случае, сеттер нужно будет закоментировать
        }
    }

    @Override
    public boolean equals(Object object){

        if (this == object) return true;

//        if (!(object instanceof Computer)) return false;
        if (object == null || getClass() != object.getClass()) return false;

        Computer other = (Computer) object;

        return ram == other.ram && Objects.equals(this.brand, brand); //(brand != null && brand.equals(other.brand)
    }

    //hashCode
    // 1. Если equals для двух объектов возвращает true, тогда hashCode этих объектов одинаковы
    // 2. Если hashCode объектов одинаковы, то equals для двух объектов не обязательно вернет true

    @Override
    public int hashCode(){
        //int result = 17;
        //result = 31 * result + ram;
        //result = 31 * result + (int) diskSize;
        //result = 31 * result + brand.hashCode();
        //return result;

        return Objects.hash(ram, diskSize, brand); //надо делать так

    }

}
