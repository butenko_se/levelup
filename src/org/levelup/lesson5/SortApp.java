package org.levelup.lesson5;

public class SortApp {

    public static void main(String[] args) {
        int[] array = { 1, 2, 3, 4};

        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);

        ArraySort arraySort = new MergeSort();
        arraySort.sort(array);

    }

}
