package org.levelup.lesson10;

public class OuterClassApp {

    OuterClass outerClass = new OuterClass();
    OuterClass.InnerClass innerClass = outerClass.new InnerClass();

}
