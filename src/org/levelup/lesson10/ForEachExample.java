package org.levelup.lesson10;

import org.levelup.lesson6.structure.OneWayList;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ForEachExample {

    public static void main(String[] args) {

        List<Integer> list = new LinkedList<>();
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);


        Iterator<Integer> iterator = list.iterator();

        while (iterator.hasNext()){
            Integer element = iterator.next();
            System.out.println(element);
            if (element == 7){
                iterator.remove();
            }
        }

        System.out.println();

        OneWayList<String> strings = new OneWayList<>();
        strings.add("s1");
        strings.add("s2");
        strings.add("s3");
        strings.add("s4");

        for (String string : strings){
            System.out.println(string);
        }

    }

}
