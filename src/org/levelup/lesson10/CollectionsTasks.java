package org.levelup.lesson10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class CollectionsTasks {

    public static void main(String[] args) {

        Collection<String> products = new ArrayList<>();
        products.add("Мяч");
        products.add("Сетка");
        products.add("Футболка");
        products.add("Штаны");
        products.add("Сыр");
        products.add("Молоко");
        products.add("Вода");
        products.add("Газировка");
        products.add("Ботинки");

        //CollectionsTasks.1
        StringPredicate predicate = new StringPredicate() {
            @Override
            public boolean check(String value) {
                return value.startsWith("m");
            }
        }; //анонимный внутренний (локальный) класс

        //CollectionsTasks.2
        StringPredicate p = new StringPredicate() {
            @Override
            public boolean check(String value) {
                return value.length() > 5;
            }
        };

        filterCollection(products, null);

        //Лямбда выражение
        //StringPredicate lengthPredicate = (val) -> {
        //    return val.length() > 5;
        //};
        StringPredicate lengthPredicate = (val) -> val.length() > 5;

        //Аргументы
        // 0 - () -> {}
        // 2 и более - (val1, val2) -> {}
        //
        //Тело метода
        // -> { return ... ;}
        // (args) -> code

        filterCollection(products, lengthPredicate);

        products.stream()
                .map(pr -> pr.length())
                .collect(Collectors.toList());

    }

    static Collection<String> filterCollection(Collection<String> originalCollection, StringPredicate predicate){
        Collection<String> filtered = new ArrayList<>();
        for (String element : originalCollection){
            if (predicate.check(element)){
                filtered.add(element);
            }
        }

        return filtered;

    }

    interface StringPredicate{
        boolean check(String value);
    }


}
