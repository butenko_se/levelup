package org.levelup.lesson4;

// Класс, от которого наслдуются - base class, или же superclass
// Класс, который наследуется - subclass
// Inheritance (Наследование) - is as
// (наследование в Java не множественное)
public class Rectangle extends Shape { //extends - ключивое слово наследования

    public Rectangle(){
        super(); //Вызов конструктора superclass. Пишется явно, если у конструктора родителя есть параметры (т.е. он не дефолтный)
        System.out.println("Вызван конструктор Rectangle");
    }

    public Rectangle(int width, int length){
        super(new int[] {width, length, width, length});
    }

    // Overriding - переопределение метода
    @Override
    public double calculatePerimeter(){
        // width * 2 + length * 2
        return sides[0] * 2 + sides[1] * 2;
    }

}
