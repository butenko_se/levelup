package org.levelup.lesson4;

public class PhoneApp {

    public static void main(String[] args) {

        Phone phone1 = new Phone("+79451234587", "Nokia");
        Phone phone2 = new Phone("+76845494256", "Siemens", 13);
        Phone phone3 = new Phone("+7499546154", "Iphone");

        System.out.println(phone1.number + " " + phone1.model);
        System.out.println(phone2.number + " " + phone2.model);
        System.out.println(phone3.number + " " + phone3.model);

        phone1.receiveCall("Bob");
        phone2.receiveCall("Elli");
        phone3.receiveCall("Jake");

        String a = phone1.getNumber();
        String b = phone2.getNumber();
        String c = phone3.getNumber();

        phone1.receiveCall("Bob", phone1.number);
        phone3.receiveCall("Jake", phone3.number);

        phone1.sendMessages(new String[]{"+78465123", "+7895613", "+7951351354"});

    }

}
