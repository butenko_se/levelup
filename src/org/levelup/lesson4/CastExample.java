package org.levelup.lesson4;

public class CastExample {

    public static void main(String[] args) {

        Square square = new Square(4);

        Rectangle rectangle = new Rectangle(5, 4);
        Rectangle rec = new Rectangle(5, 4);

        Shape shape = new Shape(new int[] {4, 6, 8, 8, 9});

        Shape[] shapes = new Shape[4];
        shapes[0] = square;
        shapes[1] = rectangle;
        shapes[2] = rec;
        shapes[3] = shape;

        ShapeService ss = new ShapeService();
        ss.printPerimeters(shapes);

        // Приведение объектов к типам классов родителей и наследников
        Rectangle recObjFromSquare = square;
        Shape shapeObjFromSquare = recObjFromSquare;
        Object objFromShape = shapeObjFromSquare;
        Rectangle recFromObj = (Rectangle) objFromShape;
        Shape shapeFromObj = (Shape) objFromShape;


    }

}
