package org.levelup.lesson4;

public class Phone {

        String number;
        String model;
        int weight;

        Phone(String number, String model, int weight){
            this.number = number;
            this.model  = model;
//            Phone(number, model);
            this.weight = weight;
        }

        Phone(String number, String model){
            this.number = number;
            this.model  = model;
        }

        Phone(){};

        void receiveCall(String name){
            System.out.println("Звонит " + name);
        }

        void receiveCall(String name, String number){
            System.out.println("Звонит " + name + " " + number);
        }

        String getNumber(){
           return number;
        }

        void sendMessages(String[] numbers){
            for (int i = 0; i != numbers.length; i++){
                System.out.println(numbers[i]);
            }
        }

}
