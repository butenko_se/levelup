package org.levelup.lesson4;

public class Reader {

    String name;
    int id;
    String fak;
    String dateOfBirth;
    String telNumber;

    void takeBook(int books){
        System.out.println(name + " взял " + books + " книги");
    }

    void takeBook(String[] titles){
        System.out.print(name + " взял:");
        for (int i = 0; i != titles.length; i++){
            System.out.println(titles[i]);
        }
    }

    void takeBook(Book[] books){
        System.out.print(name + " взял:");
        for (int i = 0; i != books.length; i++){
            System.out.println(books[i].title);
        }
    }

    void returnBook(int books){
        System.out.println(name + " вернул " + books + " книги");
    }

    void returnBook(String[] nameOfBooks){
        System.out.print(name + " вернул:");
        for (int i = 0; i != nameOfBooks.length; i++){
            System.out.println(nameOfBooks[i]);
        }
    }

    void returnBook(Book[] books){
        System.out.print(name + " вернул:");
        for (int i = 0; i != books.length; i++){
            System.out.println(books[i].title);
        }
    }

}
