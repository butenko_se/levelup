package org.levelup.lesson4.Animals;

public class Veterinarian {

    public static void main(String[] args) {

        Cat cat = new Cat();
        cat.food = "viskas";
        cat.location = "home";

        Dog dog = new Dog();
        dog.food = "meat";
        dog.location = "porch";

        Horse horse = new Horse();
        horse.food = "grass";
        horse.location = "field";


        Animal[] animals = new Animal[3];
        animals[0] = cat;
        animals[1] = dog;
        animals[2] = horse;

        for (int i = 0; i < animals.length; i++){
            treatAnimal(animals[i]);
        }

    }

    static void treatAnimal(Animal animal){
        System.out.println(animal.food);
        System.out.println(animal.location);
    }

}
