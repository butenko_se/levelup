package org.levelup.lesson4;

public class ShapeApp {

    public static void main(String[] args) {

//        Shape unknownShape = new Shape();
        Rectangle rectangle = new Rectangle(3, 6);
        for (int i = 0; i < rectangle.sides.length; i++){
            System.out.println(rectangle.sides[i]);
        }


        Square square = new Square(4);
        System.out.println("Периметр квадрата: " + square.calculatePerimeter());

    }

}
