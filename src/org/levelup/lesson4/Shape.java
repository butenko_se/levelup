package org.levelup.lesson4;

// 2 модификатора доступа, для внешних классов (у которых имя класса совпадает с именем файла, в котором написан)
// public - класс может быть использован везде
// default-package - класс может использоваться только в пакете

public class Shape {

    // Модификаторы доступа (на примере метода)
    // private - метод доступен только внутри класса (поле доступно только внутри класса)
    // default-package (private-package) - (не указываем модификатор доступа) доступ есть только внутри класса и пакета
    // protected - доступ есть только внутри класса и пакета, а также в любых классах наследниках
    // public - доступ есть везде

    int[] sides; //Значения сторон фигуры

    public Shape(){
        System.out.println("Вызван конструктор Shape");
        this.sides = new int[0];
    }

    public Shape(int[] sides){
        this.sides = sides;
    }

    public double calculatePerimeter(){
        return 0;
    }

}
