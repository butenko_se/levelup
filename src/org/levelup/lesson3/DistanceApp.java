package org.levelup.lesson3;

public class DistanceApp {

    public static void main(String[] args) {
        Point p1 = new Point(4, 6);
        Point p2 = new Point(8, 9);

        PointDistanceCalculation calc = new PointDistanceCalculation();
        double dist = calc.calculateDistance(p1, p2);

        System.out.println(dist);

    }

}
