package org.levelup.lesson3;

public class App {

    public static void main(String[] args) {

        Point pointA = new Point();

        //Запись значений в поле объекта
        pointA.x = 453;
        pointA.y = 1353;

        System.out.println("A(" + pointA.x + "," + pointA.y + ")");

        Point pointB = new Point();
        pointB.x = 12;
        pointB.y = 43;

        System.out.println("B(" + pointB.x + "," + pointB.y + ")");

        pointA.changeCoordinates(16, 14);
        System.out.println("A*(" + pointA.x + "," + pointA.y + ")");

        System.out.println();
        pointA.printPoint();
        pointB.printPoint();

        Point pointC = new Point(35, 8);
        pointC.printPoint("C");

    }
}
