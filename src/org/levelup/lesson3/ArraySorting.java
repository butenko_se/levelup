package org.levelup.lesson3;

public class ArraySorting {

    int[] bubbleSort(int[] arr) {

        int[] rArr = new int[arr.length];

        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[i]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }

        for (int n = arr.length - 1; n > 0; n--){
            rArr[n] = arr[n];
        }
        return rArr;
    }

}
