package org.levelup.lesson3;

public class Calculator {

    //сложение
    int add(int a, int b){
        return a + b;
    }

    int add(double a, double b){
        return (int) (a + b);
    }

    int add(long a, long b){
       return (int) (a + b);
    }

    //вычитание
    int sub(int a, int b){
        return a - b;
    }

    int sub(double a, double b){
        return (int) (a - b);
    }

    int sub(long a, long b){
        return (int) (a - b);
    }

    //умножение
    int mul(int a, int b){
        return a * b;
    }

    int mul(double a, double b){
        return (int) (a * b);
    }

    int mul(long a, long b){
        return (int) (a * b);
    }

    //деление
    int div(int a, int b){
        return a / b;
    }

    int div(double a, double b){
        return (int) (a / b);
    }

    int div(long a, long b){
        return (int) (a / b);
    }


}
