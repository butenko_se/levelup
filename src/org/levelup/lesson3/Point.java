package org.levelup.lesson3;

public class Point {

    //Поля класса (field)
    int x;
    int y;

    //Конструктор класса
    //Он не имеет тип возвращаемого значения
    Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    //Конструкторы можно тоже перегружать
    Point(){
        this.x = 1;
        this.y = 1;
    }

    //Метод для изменения значений x и y
    //<тип возвращаемого значения> <название метода>(<тип аргумента1> <название_аргумента>,...){тело метода}
    void changeCoordinates(int x1, int y1){
        if (x >= 0 && y >= 0){
            this.x = x;
            this.y = y;
        }
    }

    //Будет без параметров
    void printPoint(){
        System.out.println("(" + x + ", " + y + ")");
    }

    //Каждый метод имеет сигнатуру
    //Сигнатура - имя метода и его параметры (аргументы). Важен как порядок аргументов,
    //так и их типы
    //Перегрузка методов (method overloading) - методы имеют одинаковые названия, но разные сигнатуры
    void printPoint(String pointName){
        System.out.println(pointName + "(" + x + ", " + y + ")");
    }

    //(!) Сигнатура метода - не учитывается тип возвращаемого значения и наименования аргументов (параметров)

    void flip(int x, int y){
        this.x = x * -1;
        this.y = y * -1;
    }

    double calculateDistance(Point second){
        double k1 = Math.pow(x - second.x, 2);
        double k2 = Math.pow(y - second.y, 2);

        return Math.sqrt(k1 + k2);
    }

}
