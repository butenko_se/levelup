package org.levelup.lesson3;

public class ArrayUtil {

    int min(int[] array){
        ArraySorting a = new ArraySorting();
        int[] b = a.bubbleSort(array);
        return b[b.length - 1];
    }

    int max(int[] array){
        ArraySorting a = new ArraySorting();
        int[] b = a.bubbleSort(array);
        return b[0];
    }

}
