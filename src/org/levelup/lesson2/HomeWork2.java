package org.levelup.lesson2;

import javax.sound.midi.Soundbank;
import java.util.Random;
import java.util.Scanner;

public class HomeWork2 {

    public static void main(String[] args) {

        exsOne();
        exsTwo();
        exsThree();
        exsFour();
        exsFive();
        exsSix();
        exsSeven();
        exsEight();
        exsNine();
        exsTen();
        exsEleven();
    }

    public static void exsOne() {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n % 2 == 0){
            System.out.println("Число " + n + " четное");
        } else {
            System.out.println("Число " + n + " нечетное");
        }

    }

    public static void exsTwo(){

        Scanner scanner = new Scanner(System.in);
        double n1 = scanner.nextDouble();
        double n2 = scanner.nextDouble();

        if ((10 == n2) | (10 == n1)) {
            System.out.println("Одно из чисел равно 10");
        } else {
            double n10 = 10 - n1;
            double n20 = 10 - n2;
            if (n10 < 0) n10 = n10 * -1;
            if (n20 < 0) n20 = n20 * -1;

            if (n10 > n20){
                System.out.println("Второе число ближе к 10");
            } else {
                System.out.println("Первое число ближе к 10");
            }

        }
    }

    public static void exsThree(){

        int min = 5;
        int max = 155;
        int diff = max - min;
        Random random = new Random();
        int i = random.nextInt(diff + 1);

        if ((i > 25) & (i < 100)){
            System.out.println("Число " + i + " попало в интервал!");
        } else {
            System.out.println("Число " + i + " не попало в интервал!");
        }

    }

    public static void exsFour(){
        Random r = new Random();
//        int rInt = 100 + r.nextInt(1000 - 100);
//        if (rInt > 0) {
//            int digit = rInt % 10;
//            int max = exsFour(rInt / 10); //нашел решение, где используется рекурсия
//            System.out.println(Math.max(digit, max));
    }

    public static void exsFive(){

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        if (a < 0 | b < 0 | c < 0){
            System.out.println("Вводите только положительные числа!");
            return;
        }

        int arr[] = new int[]{a, b, c};
        for (int i = arr.length - 1; i > 0; i--){
            for (int j = 0; j < i; j++){
                if (arr[j] > arr[i]){
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
            }
        }

        a = arr[0];
        b = arr[1];
        c = arr[2];

        System.out.println(a + " " + b + " " + c);

    }

    public static void exsSix(){
        int i = 1000;
        while (true){
            int length = (int) (Math.log10(i) + 1);
            if (length == 4){
                System.out.println(i);
                i = i + 2;
            } else break;
        }
    }

    public static void exsSeven(){

        int a = 1;
        for (int i = 0; i <= 55; i++){
            System.out.println(a);
            a = a + 2;
        }

    }

    public static void exsEight(){

        int a = 90;
        while (true){
            if (a <= 0) break;
            System.out.println(a);
            a = a - 5;
        }

    }

    public static void exsNine(){

        int a = 2;
        for (int i = 0; i <= 20; i++){
            System.out.println(i + ")" + " " + a);
            a = a * 2;
        }

    }

    public static void exsTen(){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n < 0){
            System.out.println("Введите положительное число!");
            return;
        }
        long result = 1;
        for (int factor = 2; factor <= n; factor++){
            result = result * factor;
        }

    }

    public static void exsEleven(){
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            int n = scanner.nextInt();

            for(int i = n; i > 0; i--){
                int b = n % i;
                if(b == 0)
                    System.out.print(i+" ");
            }
        }
    }

}
