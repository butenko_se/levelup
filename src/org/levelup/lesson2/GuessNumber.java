package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {
        //Scanner - это класс, будем читать данные с клавиатуры
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number:");
        int number = sc.nextInt();

        //ГСПЧ
        Random r = new Random();
        int secretNumber = r.nextInt(4); //nextInt(4) -> [0, 4) || [0, 3]

        if(number == secretNumber){
            System.out.println("Вы угадали!");
        }
        else {
//            System.out.println("Вы не угадали! " + "SecretNumber = " + secretNumber);
        if (number > secretNumber) {
            System.out.println("Ваше число больше");
        }
        else {
            System.out.println("Ваше число меньше");
        }
        }
    }

}
