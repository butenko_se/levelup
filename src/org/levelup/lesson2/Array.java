package org.levelup.lesson2;

public class Array {

    public static void main(String[] args) {
        //<тип элементов массива>[] <имя массива>
        double[] prices = new double[3]; //колличество элементов массива
        prices[0] = 1254;
        prices[1] = 4364;
        prices[2] = 3874;

        double priceSum = 0;
        for (int i = 0; i < prices.length; i++){
            System.out.println(prices[i]);
            priceSum = priceSum + prices[i];
        }

        double averagePrice = priceSum / prices.length;
        System.out.println(averagePrice);


    }

}
