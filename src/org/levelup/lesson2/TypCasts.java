package org.levelup.lesson2;

public class TypCasts {

    //psvm or main + Tab or Enter
    public static void main(String[] args) {
        int intVar = 493;
        long longVar = intVar;             //Неявное расширяющее приведение (преобразование) типа

        short shortVar = (short) longVar; //Явное сужающее преобразование

        int value = 130;
        byte byteValue = (byte) value; //-126
        //int - bytw
        //127 - 127
        //128 - -128
        //129 - -127
        //130 - -126

    }

}
