package org.levelup.lesson9.file;

import java.io.File;
import java.io.IOException;

public class FileApp {

    public static void main(String[] args) throws IOException {


        File file = new File("text_file.txt");

        ReadFile readFile = new ReadFile();
        readFile.readAndPrintFile(file);

        File f = new File("write_file.txt");
        f.createNewFile();

        ReadFile fr = new ReadFile();
        fr.writeToFile(f,"i love java!");
        fr.readAndPrintFileAsStream(f);

    }


}
