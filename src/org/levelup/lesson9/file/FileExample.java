package org.levelup.lesson9.file;

import java.io.File;
import java.io.IOException;

public class FileExample {

    public static void main(String[] args) throws IOException {

        File textFile = new File("text_file.txt");
        File notExistingFile = new File("test.txt");

        System.out.println(textFile == null);           // false
        System.out.println(notExistingFile == null);    // false

        System.out.println(textFile.exists());          // true
        System.out.println(notExistingFile.exists());   // false

        if (!notExistingFile.exists()){
            boolean result = notExistingFile.createNewFile();
            System.out.println("Результат создания файла: " + result);
        }

        File srcDir = new File("src/");
        System.out.println("Is directory: " + srcDir.isDirectory());

        File tempDir = new File("temp/");
        boolean result = tempDir.mkdir(); // make directory - создание папки

    }

}
