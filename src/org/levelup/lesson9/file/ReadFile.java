package org.levelup.lesson9.file;

import java.io.*;

public class ReadFile {

    public void readAndPrintFile(File file){

        //FileReader / FileInputStream

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));

            String line;
            while ((line = reader.readLine()) != null){
                System.out.println(line);
            }

        } catch (IOException exception){
            System.out.println("Произошла ошибка при чтении файла " + exception.getMessage());
        } finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException exc){
                    System.out.println("Не возможно закрыть соединение с файлом " + exc.getMessage());
                }

            }
        }

    }

    public void readAndPrintFileAsStream(File file){
        // try-with-resources
        // Клсс обязан реализовыать интерфейс AutoClose

        // try (создаете объект, который реализует интерфейс AutoClose) {}
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))){

            //byte[] buffer = new byte[2048];
            byte[] buffer;
            while ((buffer = bis.readNBytes(100)).length != 0){
                System.out.println();
            }

        } catch (IOException exc){
            System.out.println("Не возможно закрыть соединение с файлом " + exc.getMessage());
        }

    }

    public void writeToFile(File file, String message){
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))){
            writer.write(message);
            writer.newLine();
        } catch (IOException exc) {
            System.out.println("Произошла ошибка при записи в файл: " + exc.getMessage());
        }
    }

}
