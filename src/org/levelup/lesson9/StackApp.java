package org.levelup.lesson9;

public class StackApp {

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>(5);
        try {
            stack.push("stack_1");
            stack.push("stack_2");
            stack.push("stack_3");
            stack.push("stack_4");
            String string = stack.pop();

        } catch (StackOverflowException e) {
            e.printStackTrace();
        } catch (EmptyStackException | NullPointerException e){
            System.out.println("Stack pustoi");
        }

    }

}
