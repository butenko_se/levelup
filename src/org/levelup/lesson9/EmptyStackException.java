package org.levelup.lesson9;

// Unchecked exception
public class EmptyStackException extends RuntimeException{
    public EmptyStackException(){
        super("Stack is empty!");
    }
}
