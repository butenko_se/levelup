package org.levelup.lesson9;

// Checked exception
public class StackOverflowException extends Exception {

    public StackOverflowException(int capacity){
        super("Stack is full. Stack capacity is: " + capacity);
    }

}
